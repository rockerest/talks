var Slides = {
    "slide": {
        activate( id, slide ){
            slide.classList.add( "active" );

            Slides.runtime.updateLocation( id );
        },
        loadActiveIframes( id, slide ){
            var frames = slide.querySelectorAll( "iframe" );
            frames.forEach( ( frame ) => {
                if( frame.getAttribute( "data-src" ) ){
                    frame.src = frame.getAttribute( "data-src" );
                }
                else{
                    frame.src = frame.src;
                }
            } );
        },
        runUserCode( id, slide ){
            if( config.afterSelect && typeof config.afterSelect == "function" ){
                config.afterSelect( id + 1, slide );
            }
        }
    },
    "runtime": {
        getLocationSlideId( location ){
            var query = window.location.hash.substr( 1 ).split( "&" );
            var id = 0;

            if( query != "" ){
                for( let i = 0; i < query.length; i++ ){
                    let parts = query[ i ].split( "=", 2 );

                    if( parts.length == 2 && parts[ 0 ] == "slide" ){
                        id = parseInt( parts[ 1 ], 10 ) - 1;
                    }
                }
            }

            return id;
        },
        updateLocation( id ){
            window.location.hash = `slide=${id + 1}`;
        },
        deactivateAllSlides(){
            if( document.querySelector( "section.active" ) ){
                document.querySelector( "section.active" ).classList.remove( "active" );
            }
        }
    },
    "helpers": {
        iframeReloaders(){
            var reloaders = document.querySelectorAll( "button[reload]" );

            reloaders.forEach( ( button ) => {
                button.addEventListener( "click", ( event ) => {
                    let id = event.target.getAttribute( "reload" );
                    let iframe = document.querySelector( `iframe#${id}` );

                    iframe.src = iframe.src;
                } );
            } );
        },
        swiping(){
            var threshold = 150;
            var allowedTime = 300;
            var restraint = 250;
            var touchsurface = document.body;
            var swipedir;
            var startX;
            var startY;
            var distX;
            var distY;
            var elapsedTime;
            var startTime;

            touchsurface.addEventListener( "touchstart", ( event ) => {
                let touchobj = event.changedTouches[ 0 ];

                swipedir = "none";
                dist = 0;
                startX = touchobj.pageX;
                startY = touchobj.pageY;
                startTime = new Date().getTime();
            }, false );

            touchsurface.addEventListener( "touchend", ( event ) => {
                let slides = document.querySelectorAll( "#slides section" );
                let activeSlide = document.querySelector( "section.active" );
                let touchobj = event.changedTouches[ 0 ];

                distX = touchobj.pageX - startX;
                distY = touchobj.pageY - startY;
                elapsedTime = new Date().getTime() - startTime;

                if( elapsedTime <= allowedTime ){
                    if( Math.abs( distX ) >= threshold && Math.abs( distY ) <= restraint ){
                        swipedir = ( distX < 0 ) ? "left" : "right";
                    }
                    else if( Math.abs( distY ) >= threshold && Math.abs( distX ) <= restraint ){
                        swipedir = ( distY < 0 ) ? "up" : "down";
                    }
                }

                if( swipedir == "left" || swipedir == "up" ){
                    Slides.navigation.nextSlide( activeSlide, slides );
                }
                else if( swipedir == "right" || swipedir == "down" ){
                    Slides.navigation.previousSlide( activeSlide, slides );
                }
            }, false);
        },
        keys(){
            document.body.addEventListener( "keydown", ( event ) => {
                let slides = document.querySelectorAll( "#slides section" );
                let activeSlide = document.querySelector( "section.active" );

                if( Slides.navigation.previousKey( event ) ){
                    Slides.navigation.previousSlide( activeSlide, slides );
                }
                else if( Slides.navigation.nextKey( event ) ){
                    Slides.navigation.nextSlide( activeSlide, slides );
                }
            }, false );
        },
        bind(){
            Slides.helpers.keys();
            Slides.helpers.iframeReloaders();
            Slides.helpers.swiping();
        }
    },
    "navigation": {
        isKey( event, keys, codes ){
            var code = event.keyCode;
            var key = event.key;

            var is = false;

            if( keys.indexOf( key ) > -1 || codes.indexOf( code ) > -1 ){
                is = true;
            }

            return is;
        },
        previousKey( event ){
            var keys = [ "ArrowUp", "ArrowLeft" ];
            var codes = [ 37, 38 ];

            return Slides.navigation.isKey( event, keys, codes );
        },
        nextKey( event ){
            var keys = [ "ArrowDown", "ArrowRight" ];
            var codes = [ 39, 40 ];

            return Slides.navigation.isKey( event, keys, codes );
        },
        previousSlide( current, slides ){
            var currIdx = Array.prototype.indexOf.call( slides, current );

            Slides.select( currIdx - 1 );
        },
        nextSlide( current, slides ){
            var currIdx = Array.prototype.indexOf.call( slides, current );

            Slides.select( currIdx + 1 );
        }
    },
    "renderer": {
        getNewSlide(){
            return document.createElement( "section" );
        },
        getSlide( el, slide ){
            var renderers = {
                "title": Slides.renderer.titleSlide,
                "section": Slides.renderer.sectionSlide,
                "default": Slides.renderer.defaultSlide,
                "undefined": Slides.renderer.defaultSlide
            };

            if( renderers[ slide.type ] ){
                renderers[ slide.type ]( el, slide );
            }

            Slides.renderer.injectSlideTitle( el, slide );
            Slides.renderer.injectSlideBody( el, slide );
        },
        titleSlide( el, slide ){
            el.classList.add( "title" );
        },
        sectionSlide( el, slide ){
            el.classList.add( "section" );
        },
        defaultSlide( el, slide ){
            // noop
        },
        injectSlideTitle( el, slide ){
            var title;

            if( ( slide.title || Slides.config.title ) || slide.subtitle ){
                title = document.createElement( "div" );
                title.classList.add( "title" );
            }

            if( slide.title || config.title ){
                title.insertAdjacentHTML( "beforeend", `<h1>${slide.title || Slides.config.title}</h1>` );
            }

            if( slide.subtitle ){
                title.insertAdjacentHTML( "beforeend", `<h2>${slide.subtitle}</h2>` );
            }

            if( title ){
                el.appendChild( title );
            }
        },
        injectSlideBody( el, slide ){
            if( slide.body ){
                el.insertAdjacentHTML( "beforeend", `<div class="body">${slide.body}</div>` );
            }
        },
        injectCustomDesign( el ){
            var designLayer = document.createElement( "div" );
            designLayer.classList.add( "design" );

            if( Slides.config.design ){
                designLayer.insertAdjacentHTML( "beforeend", Slides.config.design );
            }

            el.appendChild( designLayer );
        }
    },
    start( slides = [], config = {} ){
        var id;

        Slides.config = config;

        Slides.render( slides, config );
        id = Slides.runtime.getLocationSlideId( window.location );
        Slides.select( id );

        Slides.helpers.bind();
    },
    render( slides = [] ){
        slides.forEach( ( slide ) => {
            let el = Slides.renderer.getNewSlide();

            Slides.renderer.injectCustomDesign( el );
            Slides.renderer.getSlide( el, slide );

            document.getElementById( "slides" ).appendChild( el );
        } );
    },
    select( id = 0 ){
        var slides = document.querySelectorAll( "#slides section" );

        if( id > -1 && id < slides.length ){
            Slides.runtime.deactivateAllSlides();
            Slides.slide.activate( id, slides[ id ] );
            Slides.slide.loadActiveIframes( id, slides[ id ] );
            Slides.slide.runUserCode( id, slides[ id ] );
        }
    }
};
