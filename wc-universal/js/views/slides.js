/*
	This file stands in for your view rendering framework of choice.

	Its jobs are:
	- Render some (related) components
	- Create event bindings between components

	Note that in this implementation, there is no bound state: everything is done via events.
	It would be extremely similar to store a `slide` variable as local state and setAttribute on the slides component.
	However, this is unnecessary state, as event-based communication is completely effective.
*/

var nextHandler;
var previousHandler;

function next( slides ){
	return () => slides.dispatchEvent( new CustomEvent( "next" ) );
}

function previous( slides ){
	return () => slides.dispatchEvent( new CustomEvent( "previous" ) );
}

function hashParams(){
	let hash = (window.location.hash).replace( /^#/, "" );
	let parts = hash.split( ";" );

	return parts
		.map( ( part ) => part.split( "=" ) )
		.reduce( ( accumulator, [ key, value ] ) => {
			if( key != "" ){
				accumulator[ key ] = value;
			}

			return accumulator;
		}, {});
}

function updateHashParam( param, value ){
	var params = hashParams();

	params[ param ] = value;

	window.location.hash = Object
		.entries( params )
		.map( ( [ key, value ] ) => `${key}=${value}` )
		.join( ";" );
}

export function render(){
	var params = hashParams();
	var view = document.querySelector( ".view" );
	var slideControls = document.createElement( "slide-controls" );
	var slides = document.createElement( "slides-wrapper" );
	var slidesFooter = document.createElement( "slide-footer" );

	nextHandler = next( slides );
	previousHandler = previous( slides );

	if( params.s ){
		localStorage.setItem( "slideIndex", params.s );
		slides.setAttribute( "slide", params.s );
	}

	view.appendChild( slideControls );
	view.appendChild( slides );
	view.appendChild( slidesFooter );

	slides.addEventListener( "slide", ( event ) => {
		updateHashParam( "s", event.detail.number );
	} );

	document.body.addEventListener( "NextSlide", nextHandler );
	document.body.addEventListener( "PreviousSlide", previousHandler );
}

/* This unrender function is never used, but your view renderer would handle this, as necessary! */
export function unrender(){
	document.body.removeEventListener( "NextSlide", nextHandler );
	document.body.removeEventListener( "PreviousSlide", previousHandler );
}
