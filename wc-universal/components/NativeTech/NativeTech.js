import { Component } from "../Component.js";
import { template } from "./NativeTech.template.js";

class NativeTech extends Component{
	static get observedAttributes(){
		return [
			"tech"
		];
	}

	constructor( ...args ){
		var self = super( ...args );

		self.replaceTemplate( template );

		this.changeTech( self.getAttribute( "tech" ) );

		return self;
	}

	changeTech( newTech ){
		var techs = {
			"templates": {
				"src": "images/stamp-regular.svg",
				"text": "HTML Templates"
			},
			"custom-elements": {
				"src": "images/hexagon-regular.svg",
				"text": "Custom Elements"
			},
			"shadow-dom": {
				"src": "images/hexagon-solid.svg",
				"text": "Shadow DOM"
			},
			"es-modules": {
				"src": "images/puzzle-piece-regular.svg",
				"text": "ES Modules"
			},
			"html-imports": {
				"src": "images/file-import-regular.svg",
				"text": "HTML Imports"
			}
		};

		if( techs[ newTech ] ){
			this.shadowRoot.querySelector( "img" ).setAttribute( "src", techs[ newTech ].src );
			this.shadowRoot.querySelector( ".text" ).textContent = techs[ newTech ].text;
		}
	}
}

NativeTech.as = "native-tech";

export { NativeTech };
