import { Component } from "../Component.js";

import { template } from "./Profile.template.js";

class Profile extends Component{
	static get observedAttributes(){
		return [];
	}

	constructor( ...args ){
		var self = super( ...args );

		self.replaceTemplate( template );

		return self;
	}
}

Profile.as = "x-profile";

export { Profile };