export var template = `
<style>
	:host{
		display:block;
		position: relative;
	}

	h4{
		line-height: 1.2em;
		margin: .25em 0;
	}

	profile-picture{
		vertical-align: -0.1em;
	}

	img{
		max-height: 1em;
		vertical-align: middle;
	}
</style>
<h4>
	<profile-picture email="thomas@tomr.email"></profile-picture>
	Thomas Randolph
	<br />
	Front-end Engineer @ <img src="images/gitlab-brands.svg" />&nbsp;GitLab
</h4>
`;