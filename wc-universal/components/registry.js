import { NativeTech } from "./NativeTech/NativeTech.js";
import { ProfilePicture } from "./ProfilePicture/ProfilePicture.js";
import { Profile } from "./Profile/Profile.js";
import { SlideControls } from "./SlideControls/SlideControls.js";
import { SlideFooter } from "./SlideFooter/SlideFooter.js";
import { Slides } from "./Slides/Slides.js";
import { TimeLine } from "./TimeLine/TimeLine.js";

export function registrar(){
	customElements.define( NativeTech.as, NativeTech );
	customElements.define( ProfilePicture.as, ProfilePicture );
	customElements.define( Profile.as, Profile );
	customElements.define( SlideControls.as, SlideControls );
	customElements.define( SlideFooter.as, SlideFooter );
	customElements.define( Slides.as, Slides );
	customElements.define( TimeLine.as, TimeLine );
}
