import { Component } from "../Component.js";

import { template } from "./SlideFooter.template.js";

class SlideFooter extends Component{
	static get observedAttributes(){
		return [];
	}

	constructor( ...args ){
		var self = super( ...args );

		self.replaceTemplate( template );

		return self;
	}
}

SlideFooter.as = "slide-footer";

export { SlideFooter };