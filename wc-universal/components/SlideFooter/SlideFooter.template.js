export var template = `
<link rel="stylesheet" href="css/SlideFooter.css" />

<footer>
	<div class="left">
		<a href="https://twitter.com/rockerest">@rockerest</a>
	</div>
	<div class="right">
		<a href="https://rdl.ph/wc">https://rdl.ph/wc</a>
	</div>
</footer>
`;