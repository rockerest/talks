import { Component } from "../Component.js";
import { template } from "./Slides.template.js";

class Slides extends Component{
	static get observedAttributes(){
		return [
			"slide"
		];
	}

	constructor( ...args ){
		var self = super( ...args );

		self.replaceTemplate( template );

		self.slide = parseInt( self.getAttribute( "slide" ), 10 ) || 1;
		self.slides = Array.from( self.shadowRoot.querySelectorAll( ".slide" ) );

		self.slides.forEach( ( slide, index ) => this.hideSlide( index + 1 ) );

		return self;
	}

	connectedCallback(){
		this.addEventListener( "next", () => {
			this.changeSlide( this.slide + 1 );
		} );

		this.addEventListener( "previous", () => {
			this.changeSlide( this.slide - 1 );
		} );

		this.updateSlide( parseInt( localStorage.getItem( "slideIndex" ) || this.slide, 10 ) );
		this.showSlide( this.slide );
	}
	attributeChangedCallback( name, oldVal, newVal ){
		var handlers = {
			"slide": ( oldV, newV ) => this.changeSlide( parseInt( newV, 10 ) )
		};

		if( handlers[ name ] ){
			handlers[ name ]( oldVal, newVal );
		}
	}

	changeSlide( newNumber ){
		var num = Math.max( 1, Math.min( this.slides.length, newNumber ) );

		if( num != this.slide ){
			this.hideSlide( this.slide );
			this.updateSlide( num );
			this.showSlide( this.slide );
	
			this.dispatchEvent( new CustomEvent( "slide", {
				"detail": {
					"number": num
				}
			} ) );
		}
	}
	hideSlide( num ){
		this.slides[ this.safeSlide( num ) - 1 ].style.display = "none";
	}
	showSlide( num ){
		this.slides[ this.safeSlide( num ) - 1 ].style.display = "";
	}
	updateSlide( num ){
		this.slide = this.safeSlide( num );

		localStorage.setItem( "slideIndex", this.slide );
		this.setAttribute( "slide", this.slide );
	}
	safeSlide( num ){
		if( num > this.slides.length || num < 1 ){
			num = 1;
		}

		return num;
	}
}

Slides.as = "slides-wrapper";

export { Slides };
