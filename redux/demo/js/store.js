import rootReducer from "./reducers/root.js";

var Store = {
    init(){
        window[ window.ns ].store = Redux.createStore( rootReducer );

        return Store.get();
    },
    get(){
        return window[ window.ns ].store;
    }
};

export default Store;
