import { TYPE as ROUTE_CHANGE } from "../actions/ROUTE_CHANGE.js";

function routingReducer( state = {}, action ){
    var newState = Object.assign( {}, state );
    var routingActions = {
        [ROUTE_CHANGE]: () => {
            return {
                "current": action.context
            };
        }
    }

    if( routingActions[ action.type ] ){
        newState = routingActions[ action.type ]();
    }

    return newState;
}

export default routingReducer;
