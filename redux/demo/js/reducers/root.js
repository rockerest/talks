import routingReducer from "./routing.js";

var initialState = {
    "routing": {
        "current": {
            "name": "home"
        }
    }
};

export default function rootReducer( state = initialState, action ){
    return {
        "routing": routingReducer( state.routing, action )
    };
}
