import Store from "./store.js";

import { routeChangeCreator } from "./actions/ROUTE_CHANGE.js";

var router = page;

router( /^\/$/, ( context ) => {
    Store
        .get()
        .dispatch( routeChangeCreator( {
            "name": "home"
        } ) );
} );

router( /^\/dashboard(?:\/)?$/, ( context ) => {
    Store
        .get()
        .dispatch( routeChangeCreator( {
            "name": "dashboard"
        } ) );
} );

export default router;
