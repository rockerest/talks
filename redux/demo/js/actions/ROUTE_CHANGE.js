export const TYPE = "ROUTE_CHANGE";
export function routeChangeCreator( newContext ){
    return {
        "type": TYPE,
        "context": newContext
    };
}
