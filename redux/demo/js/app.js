import "./setup.js";
import Store from "./store.js";
import router from "./routing.js";

import HomeController from "./controllers/home.js";
import DashboardController from "./controllers/dashboard.js";

var controllers = [
    HomeController,
    DashboardController
];
var store = Store.init();

controllers.forEach( ( controller ) => {
    store.subscribe(
        () => controller.subscriber( store.getState() )
    );
} );

router();
