var HomeController = {
    act(){
        let view = document.getElementById( "main" );
        let link = document.createElement( "a" );

        link.setAttribute( "href", "/dashboard" );
        link.textContent = "Go to my dashboard";

        view.textContent = "";
        view.appendChild( link );
    },
    subscriber( state ){
        if( state.routing.current.name == "home" ){
            HomeController.act();
        }
    }
};

export default HomeController;
