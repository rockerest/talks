var DashboardController = {
    act(){
        let view = document.getElementById( "main" );
        let link = document.createElement( "a" );
        let brk = document.createElement( "br" );

        link.setAttribute( "href", "/" );
        link.textContent = "Go the the home page";

        view.textContent = "My dashboard";
        view.appendChild( brk );
        view.appendChild( link );
    },
    subscriber( state ){
        if( state.routing.current.name == "dashboard" ){
            DashboardController.act();
        }
    }
};

export default DashboardController;
