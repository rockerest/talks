import { registrar } from "../components/registry.js";
import { render as renderSlidesView } from "./views/slides.js";

registrar();

renderSlidesView();
