export var template = `

<style>
	.slide{
		box-sizing: border-box;
		height: 100vh;
		width: 100vw;
		padding: 12rem 6rem 2rem;
		text-align: center;
	}

	.slide *{
		box-sizing: inherit;
	}

	.slide h1{
		margin: 1rem;
		padding: 0;
		line-height: 1.25em;
		text-align: center;
	}

	.slide pre{
		background-color: var( --color-code );
		border-radius: var( --size-default-border-radius );
		display: inline-block;
		margin: 3rem 0 1.75rem;
		text-align: left;
	}

	.slide code{
		color: var( --color-bright );
		font-kerning: none;
		padding: var( --size-default-padding );
	}

	.slide p{
		margin: 3rem 0 1.75rem;
	}

	.slide ul{
		margin: 3rem 0 1.75rem;
		text-align: left;
	}

	.slide img{
		display: block;
		margin: auto;
		max-width: calc( 100vw - 6rem );
	}
</style>

<div class="slide">
	<h1>Web Components</h1>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<native-tech tech="templates"></native-tech>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<native-tech tech="templates"></native-tech>
	<native-tech tech="custom-elements"></native-tech>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<native-tech tech="templates"></native-tech>
	<native-tech tech="custom-elements"></native-tech>
	<native-tech tech="shadow-dom"></native-tech>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<native-tech tech="templates"></native-tech>
	<native-tech tech="custom-elements"></native-tech>
	<native-tech tech="shadow-dom"></native-tech>
	<native-tech tech="es-modules"></native-tech>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<native-tech tech="templates"></native-tech>
	<native-tech tech="custom-elements"></native-tech>
	<native-tech tech="shadow-dom"></native-tech>
	<native-tech tech="es-modules"></native-tech>
	<native-tech tech="html-imports"></native-tech>
</div>
<div class="slide">
	<native-tech tech="templates"></native-tech>
</div>
<div class="slide">
	<native-tech tech="templates"></native-tech>
	<pre><code>&lt;template&gt;&lt;/template&gt;</code></pre>
</div>
<div class="slide">
	<native-tech tech="templates"></native-tech>
	<pre><code>&lt;template&gt;&lt;/template&gt;</code></pre>
	<p>
		Parse, clone, stamp
	</p>
</div>
<div class="slide">
	<native-tech tech="templates"></native-tech>
	<pre><code>&lt;template&gt;&lt;/template&gt;</code></pre>
	<p>
		If only:
	</p>
</div>
<div class="slide">
	<native-tech tech="templates"></native-tech>
	<pre><code>&lt;template&gt;&lt;/template&gt;</code></pre>
	<p>
		If only:
	</p>
	<ul>
		<li>there was a way to have the browser natively load these</li>
	</ul>
</div>
<div class="slide">
	<native-tech tech="templates"></native-tech>
	<pre><code>&lt;template&gt;&lt;/template&gt;</code></pre>
	<p>
		If only:
	</p>
	<ul>
		<li>there was a way to have the browser natively load these</li>
		<li>without render/paint cycles...</li>
	</ul>
</div>
<div class="slide">
	<native-tech tech="html-imports"></native-tech>
</div>
<div class="slide">
	<native-tech tech="html-imports"></native-tech>
	<pre><code>&lt;link rel="import" href="User.html" /&gt;</code></pre>
</div>
<div class="slide">
	<native-tech tech="html-imports"></native-tech>
	<pre><code>&lt;link rel="import" href="User.html" /&gt;</code></pre>
	<p>
		HTML documents / document fragments
	</p>
</div>
<div class="slide">
	<native-tech tech="html-imports"></native-tech>
	<pre><code>&lt;link rel="import" href="User.html" /&gt;</code></pre>
	<p>
		Load tree (import chain), parse, execute scripts
	</p>
</div>
<div class="slide">
	<native-tech tech="html-imports"></native-tech>
	<pre><code>&lt;link rel="import" href="User.html" /&gt;</code></pre>
	<p>
		Support in 1 browser, no plans to support anywhere else
	</p>
</div>
<div class="slide">
	<native-tech tech="templates"></native-tech>
	<native-tech tech="custom-elements"></native-tech>
	<native-tech tech="shadow-dom"></native-tech>
	<native-tech tech="es-modules"></native-tech>
	<native-tech tech="html-imports" strike="true"></native-tech>
</div>
<div class="slide">
	<native-tech tech="templates" shrug="true"></native-tech>
	<native-tech tech="custom-elements"></native-tech>
	<native-tech tech="shadow-dom"></native-tech>
	<native-tech tech="es-modules"></native-tech>
	<native-tech tech="html-imports" strike="true"></native-tech>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
	<native-tech tech="shadow-dom"></native-tech>
	<native-tech tech="es-modules"></native-tech>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
	<pre>
<code>window
	.customElements
	.define(
		"my-hyphenated-name",
		MyHyphenatedClass
	);</code>
</pre>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
	<p>
		Lifecycle events: <pre><code>upgrade</code></pre> (construct), <pre><code>connect</code></pre>, <pre><code>adopt</code></pre>, <pre><code>disconnect</code></pre>, <pre><code>attributeChanged</code></pre>
	</p>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
	<p>
		<pre><code>&lt;my-hyphenated-name&gt;</code></pre> Just works
	</p>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
	<p>
		<pre><code>&lt;my-hyphenated-name&gt;</code></pre> Just works
		<br />
		<pre style="font-size: 3rem; line-height: 3.1rem;"><code>class MyHyphenatedClass extends HTMLElement{
    constructor( ...args ){
        var self = super( ...args );
        var content = document.createElement( "p" );

        content.textContent = "This is a hyphenated element";

        self.root = self.attachShadow( { "mode": "open" } );
        self.root.appendChild( content );

        return self;
    }
}

customElements.define( "my-hyphenated-name", MyHyphenatedClass );</code></pre>
		<my-hyphenated-name></my-hyphenated-name>
	</p>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
	<p>
		<strong>Pure</strong> encapsulation
	</p>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
	<p>
		<pre><code>&lt;html&gt;</code></pre> & <pre><code>&lt;body&gt;</code></pre> cascade only
	</p>
</div>
<div class="slide">
		<native-tech tech="shadow-dom"></native-tech>
		<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
		<p>
			<pre><code>&lt;html&gt;</code></pre> & <pre><code>&lt;body&gt;</code></pre> cascade only
		</p>
		<div>
			<pre><code>input{
    background-color: red;
    border: 1px solid blue;
    padding: 1rem;
}</code></pre>
			<br />
			<input /><button onclick="var v = document.querySelector( '.view' );v.insertBefore( document.createElement( 'input' ), v.firstChild )">Add non-component input</button>
			<p>&lt;p&gt;</p>
		</div>
	</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
	<p>
		New CSS root, so cascades are shallower
	</p>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
	<p>
		No external CSS leakage for selectors
	</p>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
	<p>
		No external CSS leakage for selectors
	</p>
	<div>
		<pre><code>.view{
    width: 250px;
    background-color: red;
}</code></pre>
		<style>
			.view{
				background-color: red;
				margin: auto;
				width: 250px;
			}
		</style>
		<div class="view">
			Scoped .view
		</div>
		<button onclick="document.querySelector( '.view' ).style.boxShadow = '0 0 3px 3px red inset';">highlight .view</button>
	</div>
</div>
<div class="slide">
	<native-tech tech="es-modules"></native-tech>
</div>
<div class="slide">
	<native-tech tech="es-modules"></native-tech>
	<pre><code>import x from "./y.js";</code></pre>
</div>
<div class="slide">
	<native-tech tech="es-modules"></native-tech>
	<pre><code>import x from "./y.js";</code></pre>
	<p>
		Modular encapsulation of code
	</p>
</div>
<div class="slide">
	<native-tech tech="es-modules"></native-tech>
	<pre><code>import x from "./y.js";</code></pre>
	<p>
		Deterministic dependency trees
	</p>
</div>
<div class="slide">
	<native-tech tech="es-modules"></native-tech>
	<pre><code>import x from "./y.js";</code></pre>
	<p>
		etc.
	</p>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<p>Chrome and Opera have 100% support</p>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<p>Safari supports everything except HTML Imports</p>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<p>Firefox will have support for everything except HTML Imports on October 23rd</p>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<p>Edge currently supports HTML Templates and ES Modules</p>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<p>Edge currently supports HTML Templates and ES Modules</p>
	<p>Intent to ship:</p>
	<ul>
		<li>Custom Elements</li>
	</ul>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<p>Edge currently supports HTML Templates and ES Modules</p>
	<p>Intent to ship:</p>
	<ul>
		<li>Custom Elements</li>
		<li>Shadow DOM</li>
	</ul>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<img src="images/support.png" />
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<img src="images/support2.png" />
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<pre><code>npm install @webcomponents/webcomponentsjs</code></pre>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<pre><code>npm install @webcomponents/webcomponentsjs</code></pre>
	<p>
		Polyfills for each of the things necessary for Web Components
	</p>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<pre><code>npm install @webcomponents/webcomponentsjs</code></pre>
	<p>
		Unlikely to ever get HTML Imports support natively - just use ES Modules
	</p>
</div>
<div class="slide">
	<h1>Support / Issues</h1>
	<pre><code>npm install @webcomponents/webcomponentsjs</code></pre>
	<p>
		Polyfill Firefox (until Nov.) and Edge (until ~2019?)
	</p>
</div>
<div class="slide">
	<h1>Solving Problems</h1>
</div>
<div class="slide">
	<h1>Solving Problems</h1>
	<p>
		Decouples the idea of <em>components</em> from library paradigms
	</p>
</div>
<div class="slide">
	<h1>Solving Problems</h1>
	<p>
		Decouples the idea of <em>components</em> from library paradigms
	</p>
	<p>
		Ensures team-driven autonomy of view libraries while still standardizing building blocks
	</p>
</div>
<div class="slide">
	<h1>Solving Problems</h1>
	<p>
		Bets on the platform itself (HTML, JS, CSS )
	</p>
</div>
<div class="slide">
	<h1>Solving Problems</h1>
	<p>
		Bets on the platform itself (HTML, JS, CSS )
	</p>
	<p>
		Extremely high guarantee of stability and backwards compatibility
	</p>
</div>
<div class="slide">
	<h1>Solving Problems</h1>
	<p>
		Strictly (and natively) enforces encapsulation
	</p>
</div>
<div class="slide">
	<h1>Solving Problems</h1>
	<p>
		Strictly (and natively) enforces encapsulation
	</p>
	<p>
		Components are 100% portable
	</p>
</div>
<div class="slide">
	<h1>Web Components</h1>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<ul>
		<li>Minimal polyfills for a few months</li>
	</ul>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<ul>
		<li>Minimal polyfills for a few months</li>
		<li>Many solved annoyances (like CSS isolation)</li>
	</ul>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<ul>
		<li>Minimal polyfills for a few months</li>
		<li>Many solved annoyances (like CSS isolation)</li>
		<li>Bets on the native platform</li>
	</ul>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<ul>
		<li>Minimal polyfills for a few months</li>
		<li>Many solved annoyances (like CSS isolation)</li>
		<li>Bets on the native platform</li>
		<li>Ultimate portability</li>
	</ul>
</div>
`;
