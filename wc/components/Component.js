export class Component extends HTMLElement{
	constructor( ...args ){
		var self = super( ...args );

		self.root = self.attachShadow( { "mode": "open" } );

		return self;
	}

	replaceTemplate( template ){
		Array
			.from( this.root.childNodes )
			.forEach( ( child ) => {
				this.root.removeChild( child );
			} );
		this.root.appendChild( document.createRange().createContextualFragment( template ) );
	}
}
