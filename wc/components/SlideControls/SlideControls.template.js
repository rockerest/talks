export var template = `
<style>
    .left,
    .right{
        cursor: pointer;
        position: absolute;
        top: 20px;
    }

    .left{
        left: 20px;
    }

    .right{
        right: 20px;
    }
</style>

<div class="left">🢀</div>
<div class="right">🢂</div>
`;