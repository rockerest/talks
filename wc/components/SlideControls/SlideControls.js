import { Component } from "../Component.js";
import { template } from "./SlideControls.template.js";

function previous(){
	var customEvent = new Event( "PreviousSlide" );

	document.body.dispatchEvent( customEvent );
}

function next(){
	var customEvent = new Event( "NextSlide" );

	document.body.dispatchEvent( customEvent );
}

function keyUpHandler( event, mappings ){
	if( mappings[ event.key ] ){
		mappings[ event.key ]();
	}
}

class SlideControls extends Component{
	constructor( ...args ){
		var self = super( ...args );

		self.replaceTemplate( template );

		self.globalKeybinding = ( event ) => {
			keyUpHandler( event, {
				"ArrowLeft": previous,
				"a": previous,
				"ArrowRight": next,
				"d": next
			} );
		};

		return self;
	}

	connectedCallback(){
		document.addEventListener( "keyup", this.globalKeybinding );

		this.root.querySelector( ".left" ).addEventListener( "click", previous );
		this.root.querySelector( ".right" ).addEventListener( "click", next );
	}
	disconnectedCallback(){
		document.removeEventListener( "keyup", this.globalKeybinding );
	}
}

SlideControls.as = "slide-controls";

export { SlideControls };
