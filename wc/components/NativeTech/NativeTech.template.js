export var template = `

<style>
	div h2{
		line-height: 1em;
		margin: 0;
		padding: 2rem;
		text-align: center;
	}

	div h2 div{
		display: inline-block;
	}

	img{
		display: inline-block;
		justify-self: end;
		width: 1em;
		height: 1em;
	}

	span{
		text-align: left;
		justify-self: start;
	}
</style>

<div>
	<h2>
		<img src="" />
		<div>
			<span class="text"></span>
			<span class="shrug"></span>
		</div>
	</h2>
</div>
`;