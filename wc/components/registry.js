import { SlideControls } from "./SlideControls/SlideControls.js";
import { Slides } from "./Slides/Slides.js";
import { NativeTech } from "./NativeTech/NativeTech.js";

export function registrar(){
	customElements.define( SlideControls.as, SlideControls );
	customElements.define( Slides.as, Slides );
	customElements.define( NativeTech.as, NativeTech );
}
