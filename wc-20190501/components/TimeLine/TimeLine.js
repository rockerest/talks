import { Component } from "../Component.js";

import { template } from "./TimeLine.template.js";

function showPointsIncluding( points, age ){
	var seen = false;

	points.forEach( ( pt ) => {
		pt.style.display = seen ? "none" : "block";

		seen = seen || pt.classList.contains( age );
	} );
}

class TimeLine extends Component{
	static get observedAttributes(){
		return [
			"age"
		];
	}

	constructor( ...args ){
		var self = super( ...args );

		self.replaceTemplate( template );

		this.changeAge( self.getAttribute( "age" ) );

		return self;
	}

	changeAge( newAge ){
		var blocker = this.shadowRoot.querySelector( ".blocker" );
		var points = Array.from( this.shadowRoot.querySelectorAll( ".point" ) );
		var styles = {
			"dark-ages": ( blk, pts ) => {
				blk.style.left = "0";
				blk.style.width = "100%";
				showPointsIncluding( pts, "dark-ages" );
			},
			"javascript": ( blk, pts ) => {
				blk.style.left = "22%";
				blk.style.width = "78%";
				showPointsIncluding( pts, "javascript" );
			},
			"jquery": ( blk, pts ) => {
				blk.style.left = "51.5%";
				blk.style.width = "48.5%";
				showPointsIncluding( pts, "jquery" );
			},
			"frameworks": ( blk, pts ) => {
				blk.style.left = "61.5%";
				blk.style.width = "38.5%";
				showPointsIncluding( pts, "frameworks" );
			},
			"components": ( blk, pts ) => {
				blk.style.left = "66.5%";
				blk.style.width = "33.5%";
				showPointsIncluding( pts, "components" );
			},
			"future": ( blk, pts ) => {
				blk.style.left = "100%";
				blk.style.width = "0";
				showPointsIncluding( pts, "future" );
			}
		}
		
		if( styles[ newAge ] ){
			styles[ newAge ]( blocker, points );
		}
	}
}

TimeLine.as = "time-line";

export { TimeLine };