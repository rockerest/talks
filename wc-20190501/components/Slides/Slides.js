import { Component } from "../Component.js";
import { template } from "./Slides.template.js";

class Slides extends Component{
	static get observedAttributes(){
		return [
			"slide"
		];
	}

	constructor( ...args ){
		var self = super( ...args );

		self.replaceTemplate( template );

		self.slide = parseInt( self.getAttribute( "slide" ), 10 ) || 1;
		self.slides = Array.from( self.shadowRoot.querySelectorAll( ".slide" ) );

		self.slides.forEach( ( slide, index ) => this.hideSlide( index + 1 ) );

		return self;
	}

	connectedCallback(){
		this.addEventListener( "next", () => {
			this.changeSlide( this.slide + 1 );
		} );

		this.addEventListener( "previous", () => {
			this.changeSlide( this.slide - 1 );
		} );

		this.slide = localStorage.getItem( "slideIndex" ) || this.slide;
		this.changeSlide( this.slide );
	}
	attributeChangedCallback( name, oldVal, newVal ){
		var handlers = {
			"slide": ( oldV, newV ) => this.changeSlide( parseInt( newV, 10 ) )
		};

		if( handlers[ name ] ){
			handlers[ name ]( oldVal, newVal );
		}
	}

	changeSlide( newNumber ){
		var num = Math.max( 1, Math.min( this.slides.length, newNumber ) );

		this.hideSlide( this.slide );

		this.slide = num;
		localStorage.setItem( "slideIndex", num );

		this.showSlide( this.slide );

		this.dispatchEvent( new CustomEvent( "slide", {
			"detail": {
				"number": num
			}
		} ) );
	}
	hideSlide( num ){
		this.slides[ num - 1 ].style.display = "none";
	}
	showSlide( num ){
		this.slides[ num - 1 ].style.display = "";
	}
}

Slides.as = "slides-wrapper";

export { Slides };
