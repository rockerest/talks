function html( ...args ){
	// This is just a dummy HTML tagged template literal parser
	// so I get HTML highlighting in that 👇🏻 HTML. Sorry.
	return args[ 0 ].raw;
}

export var template = html`
<link rel="stylesheet" href="css/Slides.css" />

<script>
	var slides = document.querySelector( "slides-wrapper" ).shadowRoot;
</script>

<div class="slide">
	<!-- oooOOoooOOoOooooo spooky ghost slide -->
</div>
<div class="slide">
	<h1>Web Components</h1>
	<h2>A Whole New World</h2>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<h2>A Whole New World</h2>
	<h3>JAMstack Denver</h3>
	<h3>May 01, 2019</h3>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<h2>A Whole New World</h2>
	<h3>JAMstack Denver</h3>
	<h3>May 01, 2019</h3>
	<h4 style="line-height: 1.2em;" >
		<profile-picture style="vertical-align: -0.1em;" email="thomas@tomr.email"></profile-picture>
		Thomas Randolph
	</h4>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<h2>A Whole New World</h2>
	<h3>JAMstack Denver</h3>
	<h3>May 01, 2019</h3>
	<h4 id="itme" style="line-height: 1.2em;" >
		<profile-picture style="vertical-align: -0.1em;" email="thomas@tomr.email"></profile-picture>
		Thomas Randolph
	</h4>
	<style>
		#itme:before{
			content: "it me 👉🏻";
			position: absolute;
			font-size: 0.5em;
			transform:
				translate( -90px, 65px )
				rotate( -45deg );
		}
	</style>
	<a href="http://bit.ly/tr-jamstack-wc">http://bit.ly/tr-jamstack-wc</a>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<h2>A Whole New World</h2>
	<h3>JAMstack Denver</h3>
	<h3>May 01, 2019</h3>
	<h4 id="itme" style="line-height: 1.2em;" >
		<profile-picture style="vertical-align: -0.1em;" email="thomas@tomr.email"></profile-picture>
		Thomas Randolph
	</h4>
	<a href="http://bit.ly/tr-jamstack-wc">http://bit.ly/tr-jamstack-wc</a>
	<br />
	<a href="https://gitlab.com/rockerest/talks/tree/master/wc-20190501">https://gitlab.com/rockerest/talks/tree/master/wc-20190501</a>
</div>
<div class="slide">
	<h1>Components</h1>
	<figure>
		<img src="images/soHotRightNow.gif" style="height: 70vh;" />
		<figcaption style="font-size: 0.5em; color: white; margin-top: -2em;">So hot right now</figcaption>
	</figure>
</div>
<div class="slide">
	<h1>A Brief History of the Web</h1>
</div>
<div class="slide">
	<h1>A Brief History of the Web</h1>
	<style>
		time-line{
			display: inline-block;
			margin-left: -6rem;
			width: 100vw;
		}
	</style>
	<time-line age="dark-ages"></time-line>
</div>
<div class="slide">
	<h1>A Brief History of the Web</h1>
	<time-line age="javascript"></time-line>
</div>
<div class="slide">
	<h1>A Brief History of the Web</h1>
	<time-line age="jquery"></time-line>
</div>
<div class="slide">
	<h1>A Brief History of the Web</h1>
	<time-line age="frameworks"></time-line>
</div>
<div class="slide">
	<h1>A Brief History of the Web</h1>
	<time-line age="components"></time-line>
</div>
<div class="slide">
	<h1>A Brief History of the Web</h1>
	<time-line age="future"></time-line>
</div>
<div class="slide">
	<h1>Components</h1>
	<img src="images/ytho.jpg" />
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>Decouples <em>design foundation</em> from application development</li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>Decouples <em>design foundation</em> from application development</li>
		<li>Encapsulates user interfaces into one thing</li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>Decouples <em>design foundation</em> from application development</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>Decouples <em>design foundation</em> from application development</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
		<li>Reusable</li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>Decouples <em>design foundation</em> from application development</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a title="Rich Hickey; Simple Made Easy; Strangeloop 2011" href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
		<li>Reusable</li>
		<li>Isolated</li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>Decouples <em>design foundation</em> from application development</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a title="Rich Hickey; Simple Made Easy; Strangeloop 2011" href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
		<li>Reusable</li>
		<li>Isolated</li>
	</ol>
	<div id="lies">
		<blink>LIES</blink>
	</div>
	<style>
		#lies{
			align-items: center;
			background-color: hsla( 100, 100%, 100%, 0.90 );
			display: grid;
			font-size: 25vw;
			height: 100vh;
			justify-items: center;
			position: absolute;
			top: 0;
			left: -6rem;
			width: 100vw;
		}

		blink {
			animation: 1s linear infinite blink_lol;
		}

		@keyframes blink_lol {
			0% {
				visibility: visible;
			}
			50% {
				visibility: hidden;
			}
			100% {
				visibility: hidden;
			}
		}
	</style>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>
			Decouples <em>design foundation</em> from application development
			<ul>
				<li>Same tool?</li>
				<li>Same version?</li>
				<li>Same bundler config?</li>
			</ul>
		</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a title="Rich Hickey; Simple Made Easy; Strangeloop 2011" href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
		<li>Reusable</li>
		<li>Isolated</li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>
			<span style="text-decoration: line-through;">Decouples <em>design foundation</em> from application development</span>
		</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a title="Rich Hickey; Simple Made Easy; Strangeloop 2011" href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
		<li><span style="text-decoration: line-through;">Reusable</span></li>
		<li>Isolated</li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>
			<span style="text-decoration: line-through;">Decouples <em>design foundation</em> from application development</span>
		</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a title="Rich Hickey; Simple Made Easy; Strangeloop 2011" href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
		<li><span style="text-decoration: line-through;">Reusable</span></li>
		<li>
			Isolated
			<ul>
				<li>The beginning C of CSS stands for Cascading</li>
			</ul>
		</li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>
			<span style="text-decoration: line-through;">Decouples <em>design foundation</em> from application development</span>
		</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a title="Rich Hickey; Simple Made Easy; Strangeloop 2011" href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
		<li><span style="text-decoration: line-through;">Reusable</span></li>
		<li>
			Isolated
			<ul>
				<li>The beginning C of CSS stands for Cascading</li>
				<li><pre><code>bem__is a__blight--on humanity</code></pre></li>
			</ul>
		</li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>
			<span style="text-decoration: line-through;">Decouples <em>design foundation</em> from application development</span>
		</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a title="Rich Hickey; Simple Made Easy; Strangeloop 2011" href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
		<li><span style="text-decoration: line-through;">Reusable</span></li>
		<li>
			Isolated
			<ul>
				<li>The beginning C of CSS stands for Cascading</li>
				<li><pre><code>bem__is a__blight--on humanity</code></pre></li>
				<li><pre><code>css__modules__arent__better___sdoih</code></pre></li>
			</ul>
		</li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>
			<span style="text-decoration: line-through;">Decouples <em>design foundation</em> from application development</span>
		</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a title="Rich Hickey; Simple Made Easy; Strangeloop 2011" href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
		<li><span style="text-decoration: line-through;">Reusable</span></li>
		<li><span style="text-decoration: line-through;">Isolated</span></li>
	</ol>
</div>
<div class="slide">
	<h1>Components</h1>
	<ol>
		<li>
			<span style="text-decoration: line-through;">Decouples <em>design foundation</em> from application development</span>
		</li>
		<li>
			Encapsulates user interfaces into one thing
			<ul>
				<li>Development is <strong>both</strong> simple & easy: <a title="Rich Hickey; Simple Made Easy; Strangeloop 2011" href="https://www.infoq.com/presentations/Simple-Made-Easy">Simple Made Easy</a></li>
			</ul>
		</li>
		<li><span style="text-decoration: line-through;">Reusable</span></li>
		<li><span style="text-decoration: line-through;">Isolated</span></li>
	</ol>
	<div id="leia">
		<figure>
			<img src="images/onlyHope.gif" />
			<figcaption style="font-size: 3rem; color: white; margin-top: -8rem;">Help me, Obi-Web Komponents.<br />You're my only hope.</figcaption>
		</figure>
	</div>
	<style>
		#leia{
			align-items: center;
			background-color: hsla( 100, 100%, 100%, 0.90 );
			display: grid;
			font-size: 25vw;
			height: 100vh;
			justify-items: center;
			position: absolute;
			top: 0;
			left: -6rem;
			width: calc( 100vw + 6rem );
		}

		#leia figure{
			margin: 0;
		}

		#leia figcaption{
			font-family: monospace;
			line-height: 3.2rem;
			text-shadow:
				0 0 3px black,
				-1px 0 3px black,
				1px 0 3px black,
				0 1px 3px black,
				0 -1px 2px black;
			}
	</style>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<h2>A Whole New World</h2>
	<figure>
		<img src="images/newWorld.gif" style="height: 55vh;" />
		<figcaption style="font-size: 0.5em; color: white; margin-top: -1.5em; line-height: 1.1em;">🎶 Indescribable feeling (when I use Web Components) 🎵</figcaption>
	</figure>
</div>
<div class="slide">
	<h1>DOM Detour</h1>
</div>
<div class="slide">
	<h1>DOM Detour</h1>
	<p>HTML ≠ DOM</p>
</div>
<div class="slide">
	<h1>DOM Detour</h1>
	<ol>
		<li>HTML is a static document</li>
	</ol>
</div>
<div class="slide">
	<h1>DOM Detour</h1>
	<ol>
		<li>HTML is a static document</li>
		<li>↪ The browser needs a sane way to interact with the document</li>
	</ol>
</div>
<div class="slide">
	<h1>DOM Detour</h1>
	<ol>
		<li>HTML is a static document</li>
		<li>↪ The browser needs a sane way to interact with the document</li>
		<li>
			↪ The HTML specification defines Interfaces over semantic text
			<ul>
				<li>semantic text: <pre><code>&lt;div /&gt;</code></pre></li>
				<li>interface: <pre><code>HTMLDivElement</code></pre></li>
			</ul>
		</li>
	</ol>
</div>
<div class="slide">
	<h1>DOM Detour</h1>
	<ol>
		<li>HTML is a static document</li>
		<li>↪ The browser needs a sane way to interact with the document</li>
		<li>
			↪ The HTML specification defines Interfaces over semantic text
			<ul>
				<li>semantic text: <pre><code>&lt;div /&gt;</code></pre></li>
				<li>interface: <pre><code>HTMLDivElement</code></pre></li>
			</ul>
		</li>
		<li>↪ Constructs a Document Object Model</li>
	</ol>
</div>
<div class="slide">
	<h1>DOM Detour</h1>
	<div id="thediv">I am a div</div>
	<button onclick="inspectTheDiv();">Inspect the div</button>
	<div id="thedivoutput">The div is: <span></span></div>
	<script>
		function inspectTheDiv(){
			var div = slides.querySelector( "#thediv" );
			var output = slides.querySelector( "#thedivoutput span" );

			output.textContent = div.constructor.name;
		}
	</script>
</div>
<div class="slide">
	<h1>DOM Detour</h1>
	<p>How can we build our own components if every interface</p>
	<p>class is pre-defined in the HTML specification?</p>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<native-tech tech="custom-elements"></native-tech>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<native-tech tech="custom-elements" style="vertical-align: bottom;" ></native-tech>
	<native-tech tech="shadow-dom" style="vertical-align: bottom;" ></native-tech>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
	<pre>
<code>window
	.customElements
	.define(
		"my-hyphenated-name",
		MyHyphenatedClass
	);</code>
</pre>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
	<p>
		Lifecycle events:
	</p>
	<ul>
		<li>
			<pre><code>upgrade</code></pre> (construct)
		</li>
		<li>
			<pre><code>connect</code></pre>
		</li>
		<li>
			<pre><code>adopt</code></pre>
		</li>
		<li>
			<pre><code>disconnect</code></pre>
		</li>
		<li>
			<pre><code>attributeChanged</code></pre>
		</li>
	</ul>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
	<p>
		<pre><code>&lt;my-hyphenated-name&gt;</code></pre> Just works
	</p>
</div>
<div class="slide">
	<native-tech tech="custom-elements"></native-tech>
	<p>
		<pre><code>&lt;my-hyphenated-name&gt;</code></pre> Just works
		<br />
		<pre style="font-size: 2rem; line-height: 3.1rem; margin-top: 1rem;"><code>class MyHyphenatedClass extends HTMLElement{
    constructor( ...args ){
        super( ...args );
        
        var content = document.createElement( "p" );
        content.textContent = "This is a hyphenated element";
        
        this.appendChild( content );

        return this;
    }
}

customElements.define( "my-hyphenated-name", MyHyphenatedClass );</code></pre>
		<my-hyphenated-name></my-hyphenated-name>
	</p>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
	<p>
		<strong>Pure</strong> encapsulation
	</p>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
	<p>Extremely limited cascade:<br />only things that can affect the host element</p>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
	<p>Extremely limited cascade:<br />only things that can affect the host element</p>
	<div>
		<pre><code>input{
background-color: red;
border: 1px solid blue;
padding: 1rem;
}</code></pre>
		<br />
		<input /><button onclick="var v = document.querySelector( '.view' );v.insertBefore( document.createElement( 'input' ), v.firstChild )">Add non-component input</button>
	</div>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
	<p>
		No external CSS leakage for selectors
	</p>
</div>
<div class="slide">
	<native-tech tech="shadow-dom"></native-tech>
	<pre><code>el.attachShadow( { "mode": "open" } );</code></pre>
	<p>
		No external CSS leakage for selectors
	</p>
	<div>
		<pre><code>.view{
    width: 250px;
    background-color: red;
}</code></pre>
		<style>
			.view{
				background-color: red;
				margin: auto;
				width: 250px;
			}
		</style>
		<div class="view">
			Scoped .view
		</div>
		<button onclick="document.querySelector( '.view' ).style.boxShadow = '0 0 3px 3px red inset';">highlight .view</button>
	</div>
</div>
<div class="slide">
	<h1>Support</h1>
</div>
<div class="slide">
	<h1>Support</h1>
	<p>Supported natively in all evergreen browsers</p>
</div>
<div class="slide">
	<h1>Support</h1>
	<p>Supported natively in all evergreen browsers</p>
	<p>...except Edge</p>
</div>
<div class="slide">
	<h1>Support</h1>
	<p>Supported natively in all evergreen browsers</p>
	<p>...except Edge, but it's coming</p>
</div>
<div class="slide">
	<h1>Support</h1>
	<img src="images/support.jpg" />
	<a href="https://webcomponents.org">https://webcomponents.org</a>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<ol>
		<li><span style="text-decoration: line-through;">Decoupled</span></li>
		<li>✅ Encapsulated</li>
		<li><span style="text-decoration: line-through;">Reusable</span></li>
		<li><span style="text-decoration: line-through;">Isolated</span></li>
	</ol>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<ol>
		<li>
			✅ Decoupled
			<ul>
				<li>just HTML and JS (and CSS)</li>
				<li>use whatever library you want</li>
			</ul>
		</li>
		<li>✅ Encapsulated</li>
		<li><span style="text-decoration: line-through;">Reusable</span></li>
		<li><span style="text-decoration: line-through;">Isolated</span></li>
	</ol>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<ol>
		<li>
			✅ Decoupled
			<ul>
				<li>just HTML and JS (and CSS)</li>
				<li>use whatever library you want</li>
			</ul>
		</li>
		<li>✅ Encapsulated</li>
		<li>✅ Reusable</li>
		<li><span style="text-decoration: line-through;">Isolated</span></li>
	</ol>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<ol>
		<li>
			✅ Decoupled
			<ul>
				<li>just HTML and JS (and CSS)</li>
				<li>use whatever library you want</li>
			</ul>
		</li>
		<li>✅ Encapsulated</li>
		<li>✅ Reusable</li>
		<li>
			✅ Isolated
			<ul>
				<li>no more hoops to ignore cascade</li>
			</ul>
		</li>
	</ol>
</div>
<div class="slide">
	<h1>Live Coding Demo</h1>
	<p>🤞🏻</p>
	<a target="_blank" href="https://codepen.io/pen?template=BErydo">https://codepen.io/pen?template=BErydo</a>
</div>
<div class="slide">
	<h1>Web Components</h1>
	<p>💖 5eva</p>
	<p>Questions?</p>
</div>
`;
