export var template = `
<link rel="stylesheet" href="css/TimeLine.css" />

<div></div>
<div class="blocker"></div>
<div class="point dark-ages">
	<span>👈🏻 The dark ages</span>
</div>
<div class="point javascript">
	<div class="dot"></div>
	<span>JavaScript - 1995 👆🏻</span>
</div>
<div class="point jquery">
	<div class="dot"></div>
	<span>The Age of jQuery - c. 2008 👆🏻</span>
</div>
<div class="point frameworks">
	<div class="dot"></div>
	<span>Frameworks - 2012 👆🏻</span>
</div>
<div class="point components">
	<div class="dot"></div>
	<span>Components - 2014 👆🏻</span>
</div>
<div class="point future">
	<span>Aliens, probably 👉🏻</span>
</div>
`;