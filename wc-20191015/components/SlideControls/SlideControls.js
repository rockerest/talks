import { Component } from "../Component.js";
import { template } from "./SlideControls.template.js";

function previous(){
	var customEvent = new Event( "PreviousSlide" );

	document.body.dispatchEvent( customEvent );
}

function next(){
	var customEvent = new Event( "NextSlide" );

	document.body.dispatchEvent( customEvent );
}

function keyUpHandler( event, mappings ){
	if( mappings[ event.key ] ){
		mappings[ event.key ]();
	}
}

class SlideControls extends Component{
	constructor( ...args ){
		var self = super( ...args );

		self.replaceTemplate( template );

		self.globalArrowKeybinding = ( event ) => {
			keyUpHandler( event, {
				"ArrowLeft": previous,
				"ArrowRight": next
			} );
		};
		self.globalAlphaKeybinding = ( event ) => {
			keyUpHandler( event, {
				"a": previous,
				"d": next
			} );
		};

		return self;
	}

	connectedCallback(){
		document.addEventListener( "keyup", this.globalArrowKeybinding );
		document.addEventListener( "keypress", this.globalAlphaKeybinding );

		this.shadowRoot.querySelector( ".left" ).addEventListener( "click", previous );
		this.shadowRoot.querySelector( ".right" ).addEventListener( "click", next );
	}
	disconnectedCallback(){
		document.removeEventListener( "keyup", this.globalArrowKeybinding );
		document.removeEventListener( "keypress", this.globalAlphaKeybinding );
	}
}

SlideControls.as = "slide-controls";

export { SlideControls };
