export class Component extends HTMLElement{
	constructor( ...args ){
		var self = super( ...args );

		self.attachShadow( { "mode": "open" } );

		return self;
	}

	replaceTemplate( template ){
		Array
			.from( this.shadowRoot.childNodes )
			.forEach( ( child ) => {
				this.shadowRoot.removeChild( child );
			} );
		this.shadowRoot.appendChild( document.createRange().createContextualFragment( template ) );
	}
}
