import { Component } from "../Component.js";

import { md5 } from "../../js/crypto.js";

import { template } from "./ProfilePicture.template.js";

class ProfilePicture extends Component{
	static get observedAttributes(){
		return [
			"email"
		];
	}

	constructor( ...args ){
		var self = super( ...args );

		self.replaceTemplate( template );

		this.changeEmail( self.getAttribute( "email" ) );

		return self;
	}

	changeEmail( newEmail ){
		var gravatar = `https://www.gravatar.com/avatar/${md5(newEmail)}?s=300&d=identicon`;

		this.shadowRoot.querySelector( "img" ).setAttribute( "src", gravatar );
	}
}

ProfilePicture.as = "profile-picture";

export { ProfilePicture };